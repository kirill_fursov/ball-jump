﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IronSourceLaunch : MonoBehaviour
{
    public static string uniqueUserId = "RoadEBall";
    public static string appKey = "a764ea6d";

    private void Awake()
    {
        //Dynamic config example
        IronSourceConfig.Instance.setClientSideCallbacks(true);

        IronSource.Agent.validateIntegration();       
        IronSource.Agent.init(appKey, IronSourceAdUnits.INTERSTITIAL);
        //IronSource.Agent.loadInterstitial();
    }

    private void OnApplicationPause(bool isPaused)
    {
        Debug.Log("unity-script: OnApplicationPause = " + isPaused);
        IronSource.Agent.onApplicationPause(isPaused);
    }
}
