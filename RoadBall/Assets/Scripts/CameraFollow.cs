﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField] private Transform target;
    [SerializeField] private float _offset;

    private void FixedUpdate()
    {
        if (target != null)
        {
            Vector3 desiredPosition = new Vector3(
                transform.position.x,
                transform.position.y,
                target.position.z + _offset);
            transform.position = desiredPosition;
        }
    }
}
