﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacles : MonoBehaviour
{
    private float _speed = 15;

    private void Update()
    {
        transform.Translate(Vector3.right * Time.deltaTime * _speed);

        if (transform.position.z < -2)
            Destroy(gameObject);
    }

    public float Speed(float speed) => _speed += speed;
}
