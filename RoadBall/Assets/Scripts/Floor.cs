﻿using UnityEngine;

public class Floor : MonoBehaviour
{
    [SerializeField] private GameObject[] _pieceOfFloor;
    [SerializeField] private int _speedZ;
    private Vector3 initPos = new Vector3(0,0,40);

    private void Update()
    {
        MoveZ();
    }

    private void MoveZ()
    {
        foreach (GameObject item in _pieceOfFloor)
        {
            item.transform.Translate(Vector3.back * Time.deltaTime * _speedZ);
            if (item.transform.position.z <= -6f)
                item.transform.position = initPos;
        }
    }
}
