﻿using UnityEngine;

[CreateAssetMenu(fileName ="MaterialColorsCollection", menuName = "MaterialColorsCollection")]
public class MaterialColorsCollections : ScriptableObject
{
    public MaterialColorsSet[] MaterialColorsSets;
}
