﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerParticle : MonoBehaviour
{
    [SerializeField] private GameObject _player;
    [SerializeField] private float _offset;

    private void Start()
    {
        ParticleSystem.MainModule main = GetComponent<ParticleSystem>().main;
        MaterialColorsSet randColors = MaterialColorsKeeper.Instance.GetSet();

        Gradient g;
        GradientColorKey[] gck;
        GradientAlphaKey[] gak;
        g = new Gradient();
        gck = new GradientColorKey[4];
        gck[0].color = randColors.WallColors[0];
        gck[0].time = 0.0F;
        gck[1].color = randColors.WallColors[1];
        gck[1].time = 0.285F;
        gck[2].color = randColors.WallColors[2];
        gck[2].time = 0.665F;
        gck[3].color = randColors.WallColors[0];
        gck[3].time = 1.0F;
        gak = new GradientAlphaKey[2];
        gak[0].alpha = 1.0F;
        gak[0].time = 0.0F;
        gak[1].alpha = 1.0F;
        gak[1].time = 1.0F;
        g.SetKeys(gck, gak);
        main.startColor = g;
    }   

    private void Update()
    {
        transform.position = new Vector3(_player.transform.position.x, _player.transform.position.y, _player.transform.position.z+_offset);
    }
}
