﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmplitudeMain : MonoBehaviour
{
    void Awake()
    {
        Amplitude amplitude = Amplitude.Instance;
        amplitude.logging = true;
        amplitude.init("996655419623c23bc222760cc70a69cb");
    }
}
