﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameplayManager : MonoBehaviour
{
    [SerializeField] private GameObject _gameplay;
    [SerializeField] private GameObject _mainMenu;
    [SerializeField] private GameObject _gameOver;
    [SerializeField] private GameObject _loading;
    [SerializeField] private Text _scoreTextInGame;
    [SerializeField] private Text _scoreTextInEndGame;
    [SerializeField] private Player _player;
    [SerializeField] private GameObject _playerVFX;
    [SerializeField] private SpawnManager _spawnManager;
    [SerializeField] private float _difficultCoolDown;
    [SerializeField] private Button _onceMoreButton;
    [SerializeField] private Text _bestScoreText;

    private int _bestScoreSave;
    private float _scoreForDifficultChange;
    private float _startTime;
    private float _score;
    //private bool _isGame;

    private void Awake()
    {
        if (PlayerPrefs.HasKey("ScoreText"))
        {
            _bestScoreSave = PlayerPrefs.GetInt("ScoreText");
            _bestScoreText.text = $"{_bestScoreSave}";
        }
        else
        {
            _bestScoreSave = 0;
            _bestScoreText.text = $"{_bestScoreSave}";
        }
        _player.isPlayerDeth += GameOver;

        Time.timeScale = 1f;
        _onceMoreButton.onClick.AddListener(SecondChance);    
        _mainMenu.SetActive(true);
        _gameplay.SetActive(false);
        _gameOver.SetActive(false);
        _loading.SetActive(false);
        _scoreTextInGame.text = "0";
        _scoreForDifficultChange = _difficultCoolDown;
        _playerVFX.SetActive(false);
        _player.enabled = false;
        _spawnManager.enabled = false;
    }

    public void StartGame()
    {
        Vibration.Vibrate(30);
        IronSource.Agent.loadInterstitial();        
        _player.enabled = true;
        _mainMenu.SetActive(false);        
        _gameplay.SetActive(true);
        StartCoroutine(GetCurrentTime());
        _spawnManager.Activate();
        _playerVFX.SetActive(true);
    }

    public void GameOver()
    {
        Time.timeScale = 0f;
        _playerVFX.SetActive(false);
        CheckLoadInterstitial();
        _gameplay.SetActive(false);
        _gameOver.SetActive(true);
        //StopCoroutine(GetCurrentTime());
        _scoreTextInEndGame.text = $"{(int)_score}";
    }

    public void SecondChance()
    {
        IronSource.Agent.showInterstitial();
        _player.gameObject.SetActive(true);
        _player.StartCoroutine(_player.Immortality());
        _playerVFX.SetActive(true);
        _gameplay.SetActive(true);
        _gameOver.SetActive(false);
        _onceMoreButton.transform.gameObject.SetActive(false);
        Time.timeScale = 1f;
    }

    public void ToMenu()
    {
        _gameOver.SetActive(false);
        _loading.SetActive(true);        
        if (_score > _bestScoreSave)
        {
            PlayerPrefs.SetInt("ScoreText", (int)_score);
        }
        Amplitude.Instance.logEvent("Fail", new Dictionary<string, object>() { { "Score", (int)_score } });
        StopCoroutine(GetCurrentTime());
        SceneManager.LoadScene(0);
    }

    public void MultiplyFinalScore()
    {
        _score *= 2;
        _scoreTextInGame.text = $"{(int)_score}";
    }

    public void CheckLoadInterstitial()
    {
        if (IronSource.Agent.isInterstitialReady())
        {
            _onceMoreButton.transform.gameObject.SetActive(true);
        }
        else
        {
            _onceMoreButton.transform.gameObject.SetActive(false);
        }
    }

    private IEnumerator GetCurrentTime()
    {
        _startTime = Time.time;
        while (true)
        {
            yield return new WaitForSeconds(1);
            _score = Time.time - _startTime;
            --_scoreForDifficultChange;
            _scoreTextInGame.text = $"{(int)_score}";
            _spawnManager.GetTime();
        }
    }
}
