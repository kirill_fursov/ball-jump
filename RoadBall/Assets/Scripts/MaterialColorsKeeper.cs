﻿using System;
using UnityEngine;

public class MaterialColorsKeeper : MonoBehaviour
{
    [SerializeField]
    private MaterialColorsCollections _colorCollections;

    private MaterialColorsSet? _currentSet;

    public static MaterialColorsKeeper Instance{get;private set;}

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
    }

    public MaterialColorsSet GetSet()
    {
        if (!_currentSet.HasValue)
        {
            _currentSet = GetRandomSet();
        }
        return _currentSet.Value;
    }

    private MaterialColorsSet? GetRandomSet()
    {
        int count = _colorCollections.MaterialColorsSets.Length;
        int randIndex = UnityEngine.Random.Range(0, count);
        MaterialColorsSet materialColorsSet = _colorCollections.MaterialColorsSets[randIndex];
        return materialColorsSet;
    }
}
