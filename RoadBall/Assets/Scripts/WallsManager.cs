﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class WallsManager : MonoBehaviour
{
    [System.Serializable]
    public class SomeMaterials { public Material[] _colorForWalls; }

    [SerializeField] private GameObject[] _leftSideWalls;
    [SerializeField] private GameObject[] _rightSideWalls;
    [SerializeField] private int _speedZ;    
    [SerializeField] private Rigidbody _wallsRigidbody;

 
    private void Start()
    {
        MaterialColorsSet randColors = MaterialColorsKeeper.Instance.GetSet();
        ChangeColor(randColors);
    }


    public void StartWallsMoving()
    {   
        Move();
    }

    public void ChangeColor(MaterialColorsSet colors)
    {
        SetWallColors(_leftSideWalls, colors);
        SetWallColors(_rightSideWalls, colors);
    }


    private void Move()
    {
        _wallsRigidbody.velocity = Vector3.back * _speedZ;
    }

    private void SetWallColors(GameObject[] walls, MaterialColorsSet colors)
    {
        for (int i = 0; i < walls.Length; i++)
        {
            int wallIndex = i % 3;
            walls[i].GetComponent<MeshRenderer>().material.color = colors.WallColors[wallIndex];
        }
    }
}