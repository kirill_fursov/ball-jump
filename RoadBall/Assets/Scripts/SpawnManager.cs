﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    [SerializeField] private Block[] _blocks;
    [SerializeField] private float _coolDown = 2;
    [SerializeField] private WallsManager _wallsManager;
    [SerializeField] private Difficulty _difficulty;    

    private int _timeCounter;
    private int _difficultyCounter;
    private List<int> _percentages = new List<int> {0,0};

    private void Awake()
    {
        for (int i = 0; i < _difficulty.NewParametrs[_difficultyCounter].DifficultyPercent.Length; i++)
            _percentages[i] = _difficulty.NewParametrs[_difficultyCounter].DifficultyPercent[i];
    }

    public void Activate()
    {
        _wallsManager.StartWallsMoving();

        MaterialColorsSet colors = MaterialColorsKeeper.Instance.GetSet();

        StartCoroutine(CreateObstacles(colors));

    }

    public void GetTime()
    {
        _timeCounter++;
        if (_timeCounter >= 5)
        {
            if (_difficultyCounter > _difficulty.NewParametrs.Length)
                _difficultyCounter = _difficulty.NewParametrs.Length - 1;

            _coolDown = _difficulty.NewParametrs[_difficultyCounter].SpawnRate;
            for (int i = 0; i < _difficulty.NewParametrs[_difficultyCounter].DifficultyPercent.Length; i++)
                _percentages[i] = _difficulty.NewParametrs[_difficultyCounter].DifficultyPercent[i];

            _difficultyCounter++;
            _timeCounter = 0;            
        }
    }

    IEnumerator CreateObstacles(MaterialColorsSet colors)
    {
        while (true)
        {
            yield return new WaitForSeconds(_coolDown);

            float random = Random.Range(1, 100);
            int numBlockCollection = 0;

            if (random <= _percentages[0])
                numBlockCollection = 0;
            else if (random > _percentages[0] && random < _percentages[1])
                numBlockCollection = 1;
            else if (random >= _percentages[1])
                numBlockCollection = 2;

            Block currentBlock = _blocks[numBlockCollection];
            int randomBlock = Random.Range(0, currentBlock.Blocks.Length);
            var parametrsCurrentBlock = currentBlock.Blocks[randomBlock];

            GameObject obstacle = parametrsCurrentBlock.blockPrefab;
            float posX = Random.Range(parametrsCurrentBlock.borderSpawnMin, parametrsCurrentBlock.borderSpawnMax);            

            Vector3 spawnPos = new Vector3(posX, obstacle.transform.position.y, transform.position.z);

            GameObject spawnedBlock= Instantiate(obstacle, spawnPos, Quaternion.Euler(-90, 0, 90));

            spawnedBlock.GetComponent<MeshRenderer>().material.color = colors.WallColors[numBlockCollection];

        }
    }
}