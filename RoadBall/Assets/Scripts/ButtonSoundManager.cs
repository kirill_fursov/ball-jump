﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonSoundManager : MonoBehaviour
{
    [SerializeField] private Button _startButton;
    [SerializeField] private Button _onceMoreButton;
    [SerializeField] private Button _finalDigitScoreButton;
    [SerializeField] private Button _toMenuButton;
    [SerializeField] private Button _toMenuFromGameplay;
    [SerializeField] private AudioClip _buttonClickSound;
    private AudioSource _audioSource;

    private void Start()
    {
        _audioSource = GetComponent<AudioSource>();
        _startButton.onClick.AddListener(PlaySound);
        _onceMoreButton.onClick.AddListener(PlaySound);
        _finalDigitScoreButton.onClick.AddListener(PlaySound);
        _toMenuButton.onClick.AddListener(PlaySound);
        _toMenuFromGameplay.onClick.AddListener(PlaySound);
    }

    private void PlaySound()
    {
        _audioSource.PlayOneShot(_buttonClickSound);
    }
}
