﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;

public class Player : MonoBehaviour
{
    public Action isPlayerDeth;

    [SerializeField] private int _speedRotation = 5;
    [Range(0.1f, 0.5f)] [SerializeField] private float _turnSpeed;
    [SerializeField] private float _jumpHeight = 300f;
    [SerializeField] private int _immortalCooldown = 2;
    [SerializeField] private AudioClip[] _clips;
    private Rigidbody _rigidbody;
    private float _border = 1.9f;
    private Vector3 _newPos;
    private bool _immortal;
    private float _timerImmortal;
    private Color _startColor;
    private MeshRenderer _meshRenderer;

    private void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
        _meshRenderer = GetComponent<MeshRenderer>();
        _startColor = _meshRenderer.material.color;
    }

    private void Update()
    {
        Jump();
        QuickFall();
        Control();        
    }

    private void Control()
    {
        if (Time.time != 0f)
            transform.Rotate(_speedRotation, 0, 0);  

        if (Input.GetMouseButton(0) && !EventSystem.current.IsPointerOverGameObject())
        {            
            _newPos = Input.mousePosition;

            _newPos.x = -(_border - ((3.8f / Screen.width) * _newPos.x));
            _newPos.x = Mathf.Clamp(_newPos.x, -_border, _border);
            _newPos.y = transform.position.y;
            _newPos.z = transform.position.z;

            //lose control when ball jump
            if (transform.position.y <= 0.51f)
                transform.position = Vector3.Lerp(transform.position, _newPos, _turnSpeed);
        }
    }

    private void QuickFall()
    {
        if (transform.position.y > 1f && SwipeInput.Instance.SwipeDown)
        {
            _rigidbody.AddForce(new Vector2(0, -_jumpHeight * 2));
            Vibration.Vibrate(50);
        }
    }

    private void Jump()
    {
        if (transform.position.y <= 0.51f && SwipeInput.Instance.SwipeUp)
        {
            _rigidbody.AddForce(new Vector2(0, _jumpHeight));
        }
    }    

    private void OnTriggerEnter(Collider other)
    {        
        if (other.CompareTag("Obstacle"))
        {
            Destroy(other.gameObject);
            if (!_immortal)
            {
                isPlayerDeth?.Invoke();
                gameObject.SetActive(false);
            }
            Vibration.Vibrate(300);
        }
    }

    public IEnumerator Immortality()
    {
        _immortal = true;
        while (_immortalCooldown > _timerImmortal)
        {
            _meshRenderer.material.color = Color.white;
            yield return new WaitForSeconds(0.2f);
            _meshRenderer.material.color = _startColor;
            yield return new WaitForSeconds(0.2f);

            _timerImmortal+=0.5f;
        }

        _immortal = false;
        _meshRenderer.material.color = _startColor;
    }
}
