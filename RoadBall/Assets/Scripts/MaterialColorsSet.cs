﻿using System;
using UnityEngine;


[Serializable]public struct MaterialColorsSet { public Color[] WallColors; }
