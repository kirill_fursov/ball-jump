﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Difficulty Parametr",menuName = "Create Difficulty Parametr")]
public class Difficulty : ScriptableObject
{
    
    [System.Serializable]
    public class Parametrs
    {
        public float SpawnRate;
        public int[] DifficultyPercent;
    }

    public Parametrs[] NewParametrs;
}
