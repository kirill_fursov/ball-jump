﻿using UnityEngine;

public class Wall : MonoBehaviour
{    
    [SerializeField] private float _speedY = 2f;
    [SerializeField] private float _rangeY = 1.5f;
    private Vector3 initialPos;
    private int direction = 1;

    private void Awake()
    {
        initialPos = new Vector3(transform.position.x,0,transform.position.z);
    }

    private void Update()
    {
        MoveY();
        if (transform.position.z <= -0.5f)
                transform.position = new Vector3(transform.position.x, Random.Range(-1f, 2f), 39.5f);
    }

    private void MoveY()
    {
        float movementY = _speedY * Time.deltaTime * direction;
        float newY = transform.position.y + movementY;

        if (Mathf.Abs(newY - initialPos.y) > _rangeY)
            direction *= -1;
        else
            transform.position += new Vector3(0, movementY, 0);
    } 
}
