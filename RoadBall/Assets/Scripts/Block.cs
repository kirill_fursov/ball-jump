﻿using UnityEngine;

[CreateAssetMenu(fileName = "Blocks", menuName = "Create Enemies Blocks")]
public class Block : ScriptableObject
{

    [System.Serializable]
    public class Parametrs
    {
        public GameObject blockPrefab;
        [Range(-2, 0)] public float borderSpawnMin;
        [Range(0, 2)] public float borderSpawnMax;
        public Material[] materialsForAllSets;
    }

    public Parametrs[] Blocks;
}
